FROM node:20-alpine
RUN apk add --no-cache git
RUN git clone https://gitlab.com/juanluisriveramillan2/Victor.git
WORKDIR /Victor
RUN npm install
CMD npm start